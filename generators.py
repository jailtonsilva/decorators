'''a generator is a function that returns an object (iterator) 
which we can iterate over (one value at a time)'''

def simple_generator():
    a = 1
    yield a

    a += 1
    yield a

    a += 1
    yield a

a = simple_generator()

print(a)  # Returns a generator object
print(next(a))  # Returns the value of the first yield: 1
print(next(a))  # Returns the value of the second yield: 2
print(next(a))  # Returns 3
print(next(a))  # Throw error StopIteration
