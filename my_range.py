def my_range(n):
    """
    this function is equivalent to built-in range(n) function
    """
    c = 0
    while c < n:
        yield c
        c += 1

if __name__ == '__main__':
    for i in my_range(5):
        print('this is my new_range function, loop', i)